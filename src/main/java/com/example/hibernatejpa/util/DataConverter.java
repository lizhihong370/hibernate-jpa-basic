package com.example.hibernatejpa.util;

import com.example.hibernatejpa.dto.ArticleDto;
import com.example.hibernatejpa.entity.Article;

import java.util.stream.Collectors;

public class DataConverter {

    public static ArticleDto articleToDto(Article article) {
        return new ArticleDto(article.getId(),
                article.getTitle(),
                article.getCategory(),
                article.getDate(),
                article.getAuthorList().stream().map(author -> author.getName()).collect(Collectors.toList()));
    }

}
