package com.example.hibernatejpa.dao;


import com.example.hibernatejpa.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ArticleRepository extends JpaRepository<Article,Integer>,IArticleCustomized {
    List<Article> findAllByTitleAndAndCategory(String title,String category);

//    The String inside @Query is not common SQL, something like hybrid SQL and Java
    @Query("select a from Article a where a.category = 'Spring'")
    List<Article> getAllSpringArticles();
}
