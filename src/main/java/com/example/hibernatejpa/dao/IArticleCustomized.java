package com.example.hibernatejpa.dao;


import com.example.hibernatejpa.entity.Article;

import java.util.List;

public interface IArticleCustomized {
    List<Article> getReallyComplexArticles();
}
