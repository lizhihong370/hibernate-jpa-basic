package com.example.hibernatejpa.dao;


import com.example.hibernatejpa.entity.Article;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * This class is served as additional implementation for ArticleRepository
 * when there are complex queries are needed, use EntityManager to achieve
 * NOTE: the name of the class is defined on purpose, i.e.  ArticleRepository + Impl
 */
@Component
public class ArticleRepositoryImpl implements IArticleCustomized {

    @PersistenceContext
    private EntityManager entityManager;

    private static final String ALL_GIT_SQL = "From Article as a " +
            "Where a.category='Git'";


    //Usually only super complex query will put separately inside this class
    //like nested sub-query
    //Just testing here, so the sql is simple
    @SuppressWarnings("unchecked")
    @Override
    public List<Article> getReallyComplexArticles() {
        return (List<Article>) entityManager.createQuery(ALL_GIT_SQL).getResultList();
    }
}
