package com.example.hibernatejpa.controller;

import com.example.hibernatejpa.dto.ArticleDto;
import com.example.hibernatejpa.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;


@RestController
@RequestMapping("/")
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @RequestMapping(value = "article/{id}",method = RequestMethod.GET)
    public ArticleDto getArticleById(@PathVariable("id") Integer id) {
        return articleService.getArticleById(id);
    }

    @RequestMapping(value = "articles",method = RequestMethod.GET)
    public List<ArticleDto> getAllArticles() {
        return articleService.getAllArticles();
    }

    @RequestMapping(value = "article",method = RequestMethod.POST)
    public void addArticle(@RequestBody ArticleDto article, UriComponentsBuilder builder) {
        boolean flag = articleService.addArticle(article);

    }

    @RequestMapping(value = "article",method = RequestMethod.PUT)
    public ArticleDto updateArticle(@RequestBody ArticleDto article) {
        articleService.updateArticle(article);
        return article;
    }

    @RequestMapping(value = "article/{id}",method = RequestMethod.DELETE)
    public void deleteArticle(@PathVariable("id") Integer id) {
        articleService.deleteArticle(id);
    }

    @RequestMapping(value = "allGit",method = RequestMethod.GET)
    public List<ArticleDto> allGitArticles(){
        return articleService.getAllGitArticles();
    }


    @RequestMapping(value = "allSpring",method = RequestMethod.GET)
    public List<ArticleDto> allSpringArticles(){
        return articleService.getAllSpringArticles();
    }
} 