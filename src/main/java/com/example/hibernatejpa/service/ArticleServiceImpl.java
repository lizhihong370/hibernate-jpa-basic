package com.example.hibernatejpa.service;

import com.example.hibernatejpa.dao.ArticleRepository;
import com.example.hibernatejpa.dao.ArticleRepositoryImpl;
import com.example.hibernatejpa.dto.ArticleDto;
import com.example.hibernatejpa.entity.Article;
import com.example.hibernatejpa.util.DataConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ArticleRepositoryImpl articleRepositoryImpl;

    @Override
    public ArticleDto getArticleById(int articleId) {
        return DataConverter.articleToDto(articleRepository.findOne(articleId));
    }

    @Override
    public List<ArticleDto> getAllArticles() {
        List<Article> articleList = articleRepository.findAll();
        System.out.println("=== articleList : " + articleList.size() + " ====");
        return articleList.stream()
                .map(DataConverter::articleToDto)
                .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ArticleDto> getAllGitArticles() {
//        return articleRepository.getReallyComplexArticles();
        return articleRepositoryImpl.getReallyComplexArticles().stream()
                .map(DataConverter::articleToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ArticleDto> getAllSpringArticles() {
        return articleRepository.getAllSpringArticles().stream()
                .map(DataConverter::articleToDto)
                .collect(Collectors.toList());
    }

    @Override
    public synchronized boolean addArticle(ArticleDto article) {
        if (articleRepository.findAllByTitleAndAndCategory(article.getTitle(), article.getCategory()).size() > 0) {
            return false;
        } else {
//            articleRepository.saveAndFlush(article);
            return true;
        }
    }

    @Override
    public void updateArticle(ArticleDto article) {
//        articleRepository.save(article);
    }

    @Override
    public void deleteArticle(int articleId) {
        articleRepository.delete(articleId);
    }
}
