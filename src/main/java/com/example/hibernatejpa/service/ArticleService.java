package com.example.hibernatejpa.service;

import com.example.hibernatejpa.dto.ArticleDto;

import java.util.List;



public interface ArticleService {
     List<ArticleDto> getAllArticles();
     List<ArticleDto> getAllGitArticles();//For testing
     List<ArticleDto> getAllSpringArticles();

     ArticleDto getArticleById(int articleId);
     boolean addArticle(ArticleDto article);
     void updateArticle(ArticleDto article);
     void deleteArticle(int articleId);
}
