package com.example.hibernatejpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "author_detail",schema = "hibernate_play")
@Data
public class AuthorDetail implements Serializable {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_detail_generator")
    @SequenceGenerator(name = "author_detail_generator", sequenceName = "author_detail_sequence")
    int id;

    @Column(name = "address")
    String address;

    @Column(name = "description")
    String description;

    @OneToOne
    @MapsId
    @JoinColumn(name = "author_id")
    Author author;

}
