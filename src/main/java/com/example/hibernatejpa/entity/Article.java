package com.example.hibernatejpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "article",schema = "hibernate_play")
@Data
public class Article implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "article_generator")
    @SequenceGenerator(name = "article_generator", sequenceName = "article_sequence")
    @Column(name = "id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "category")
    private String category;

    @Temporal(TemporalType.DATE)
    @Column(name = "date")
    private Date date;


    @ManyToMany(mappedBy = "articles",fetch = FetchType.EAGER)
    List<Author> authorList;


} 