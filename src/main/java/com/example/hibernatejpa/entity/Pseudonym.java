package com.example.hibernatejpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "pseudonym",schema = "hibernate_play")
@Data
public class Pseudonym implements Serializable {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pseudonym_generator")
    @SequenceGenerator(name = "pseudonym_generator", sequenceName = "pseudonym_sequence")
    int id;

    @Column(name = "name")
    String name;

    @ManyToOne
    @JoinColumn(name = "author_id")
    Author author;
}
