package com.example.hibernatejpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "author",schema = "hibernate_play")
@Data
public class Author implements Serializable {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_generator")
    @SequenceGenerator(name = "author_generator", sequenceName = "author_sequence")
    int id;

    @Column(name = "name")
    String name;

    @Column(name = "birthday")
    Date birthday;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "author_article",
            joinColumns = @JoinColumn(name = "author_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "article_id", referencedColumnName = "id"))
    List<Article> articles;

    @OneToOne(mappedBy = "author", cascade = CascadeType.ALL)
    AuthorDetail authorDetail;

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
    @OrderBy("name asc")
    List<Pseudonym> pseudonymList;
}
