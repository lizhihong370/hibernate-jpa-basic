package com.example.hibernatejpa.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ArticleDto {
    private int id;

    private String title;

    private String category;

    private Date date;

    List<String> authorList;

    public ArticleDto(int id, String title, String category, Date date, List<String> authorList) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.date = date;
        this.authorList = authorList;
    }
}
